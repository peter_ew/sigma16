#include<stdio.h>
#include<string.h>
/*
	===Global variables===
*/
char** addc; //string array for labels
int* addi; //int array for labels' memory addresses
int adds=0; //counter for the labels/addresses
int fsize; //source file size
int row; //line counter
/*
	===Functions===
*/
/*
Function: checkargs1
Usage:	Checks for the validity of the arguments for an RRR instruction
		with 3 arguments and returns the ids of the corresponding registers in hex
Arguments:
arg			string holding the arguments
argl		the length of the string
reg1		first register in hex
reg2		second register in hex
reg3		third register in hex
*/
bool checkargs1(char* arg, int argl,char& reg1,char& reg2,char& reg3) {
	char temp[64];
	int i=0;
	if(arg[i]!='R') return false;
	i++;
	if(arg[i]>='0' && arg[i]<='9') {
		if(arg[i+1]>='0' && arg[i+1]<='9') {
			sprintf(temp,"%x",(arg[i]-'0')*10+(arg[i+1]-'0'));
			reg1=temp[0];
			i++;
		}
		else {
			reg1=arg[i];
		}
	}
	else
		return false;
	i++;
	if(arg[i]!=',') return false;
	i++;
	if(arg[i]!='R') return false;
	i++;
	if(arg[i]>='0' && arg[i]<='9') {
		if(arg[i+1]>='0' && arg[i+1]<='9') {
			sprintf(temp,"%x",(arg[i]-'0')*10+(arg[i+1]-'0'));
			reg2=temp[0];
			i++;
		}
		else {
			reg2=arg[i];
		}
	}
	else
		return false;
	i++;
	if(arg[i]!=',') return false;
	i++;
	if(arg[i]!='R') return false;
	i++;
	if(arg[i]>='0' && arg[i]<='9') {
		if(arg[i+1]>='0' && arg[i+1]<='9') {
			sprintf(temp,"%x",(arg[i]-'0')*10+(arg[i+1]-'0'));
			reg3=temp[0];
			i++;
		}
		else {
			reg3=arg[i];
		}
	}
	else
		return false;
	i++;
	if(i!=argl) return false;
	return true;
}
/*
Function: checkargs2
Usage:	Checks for the validity of the arguments for an RRR instruction
		with 2 arguments and returns the ids of the corresponding registers in hex
Arguments:
arg			string holding the arguments
argl		the length of the string
reg1		first register in hex
reg2		second register in hex
*/
bool checkargs2(char* arg, int argl,char& reg1,char& reg2) {
	char temp[64];
	int i=0;
	if(arg[i]!='R') return false;
	i++;
	if(arg[i]>='0' && arg[i]<='9') {
		if(arg[i+1]>='0' && arg[i+1]<='9') {
			sprintf(temp,"%x",(arg[i]-'0')*10+(arg[i+1]-'0'));
			reg1=temp[0];
			i++;
		}
		else {
			reg1=arg[i];
		}
	}
	else
		return false;
	i++;
	if(arg[i]!=',') return false;
	i++;
	if(arg[i]!='R') return false;
	i++;
	if(arg[i]>='0' && arg[i]<='9') {
		if(arg[i+1]>='0' && arg[i+1]<='9') {
			sprintf(temp,"%x",(arg[i]-'0')*10+(arg[i+1]-'0'));
			reg2=temp[0];
			i++;
		}
		else {
			reg2=arg[i];
		}
	}
	else
		return false;
	i++;
	if(i!=argl) return false;
	return true;
}
/*
Function: checkargs3
Usage:	Checks for the validity of the arguments for an RX instruction
		with 2 arguments and returns the ids of the corresponding registers in hex
Arguments:
arg			string holding the arguments
argl		the length of the string
reg1		first register in hex
reg2		second register in hex
addr        memory address of the second argument
*/
bool checkargs3(char* arg,int argl,char& reg1,char& reg2,short& addr) {
	char temp[64];
	char lookfor[64];
	int lookforl=0;
	lookfor[lookforl]='\0';
	int i=0;
	if(arg[i]!='R') return false;
	i++;
	if(arg[i]>='0' && arg[i]<='9') {
		if(arg[i+1]>='0' && arg[i+1]<='9') {
			sprintf(temp,"%x",(arg[i]-'0')*10+(arg[i+1]-'0'));
			reg1=temp[0];
			i++;
		}
		else {
			reg1=arg[i];
		}
	}
	i++;
	if(arg[i]!=',') return false;
	i++;
	while(arg[i]!='[' && i<argl) {
		lookfor[lookforl++]=arg[i++];
		lookfor[lookforl]='\0';
	}
	if(i==argl) return false;
	i++;
	if(arg[i]!='R') return false;
	i++;
	if(arg[i]>='0' && arg[i]<='9') {
		if(arg[i+1]>='0' && arg[i+1]<='9') {
			sprintf(temp,"%x",(arg[i]-'0')*10+(arg[i+1]-'0'));
			reg2=temp[0];
			i++;
		}
		else {
			reg2=arg[i];
		}
	}
	i++;
	if(arg[i]!=']') return false;
	i++;
	if(i!=argl) return false;
	if(lookfor[0]>='0' && lookfor[0]<='9') {
		int num=0;
		for(i=0;i<lookforl;i++) {
			if(lookfor[i]<'0' || lookfor[i]>'9')
				return false;
			num=num*10+(lookfor[i]-'0');
		}
		addr=num;
	}
	else {
		addr=-1;
		for(i=0;i<adds;i++)
			if(strcmp(lookfor,addc[i])==0) {
				addr=addi[i];
				break;
			}
		if(addr==-1)
			return false;
	}
	return true;
}
/*
Function: checkargs4
Usage:	Checks for the validity of the arguments for an RX instruction
		with 1 argument and returns the ids of the corresponding registers in hex
Arguments:
arg			string holding the arguments
argl		the length of the string
reg1		first register
addr        memory address of the first argument
*/
bool checkargs4(char* arg,int argl,char& reg1,short& addr) {
	char temp[64];
	char lookfor[64];
	int lookforl=0;
	lookfor[lookforl]='\0';
	int i=0;
	while(arg[i]!='[' && i<argl) {
		lookfor[lookforl++]=arg[i++];
		lookfor[lookforl]='\0';
	}
	if(i==argl) return false;
	i++;
	if(arg[i]!='R') return false;
	i++;
	if(arg[i]>='0' && arg[i]<='9') {
		if(arg[i+1]>='0' && arg[i+1]<='9') {
			sprintf(temp,"%x",(arg[i]-'0')*10+(arg[i+1]-'0'));
			reg1=temp[0];
			i++;
		}
		else {
			reg1=arg[i];
		}
	}
	i++;
	if(arg[i]!=']') return false;
	i++;
	if(i!=argl) return false;
	if(lookfor[0]>='0' && lookfor[0]<='9') {
		int num=0;
		for(i=0;i<lookforl;i++) {
			if(lookfor[i]<'0' || lookfor[i]>'9')
				return false;
			num=num*10+(lookfor[i]-'0');
		}
		addr=num;
	}
	else {
		addr=-1;
		for(i=0;i<adds;i++)
			if(strcmp(lookfor,addc[i])==0) {
				addr=addi[i];
				break;
			}
		if(addr==-1)
			return false;
	}
	return true;
}
/*
Function: getLabelName
Usage:	Gets the name of the label at the current row
Arguments:
buf			string buffer of the input file
i			current position in the input file
name		pointer to the string that is to be returned
namel		pointer to the length of the string
*/
bool getLabelName(char* buf,int& i,char* name,int& namel) {
	int nline=false;
	while(1) {
		if(i==fsize) {
			nline=true;
			break;
		}
		if(buf[i]==' ' || buf[i]=='\t')
			break;
		if(buf[i]=='\r' && buf[i+1]=='\n') {
			i+=2;
			nline=true;
			break;
		}
		if(buf[i]=='\n' || buf[i]=='\r') {
			i++;
			nline=true;
			break;
		}
		if(buf[i]==';')
			break;
		else {
			name[namel++]=buf[i];
			name[namel]='\0';
			i++;
		}
	}
	return nline;
}
/*
Function: getInstrName
Usage:	Gets the name of the instruction at the current row
Arguments:
buf			string buffer of the input file
i			current position in the input file
instr		pointer to the string that is to be returned
instrl		pointer to the length of the string
*/
bool getInstrName(char* buf,int &i,char* instr,int& instrl) {
	bool error=false;
	while(1) {
		if(buf[i]==';') {
			printf("Unexpected comment - line %d!\n",row);
			error=true;
			break;
		}
		if(buf[i]=='\r' || buf[i+1]=='\n') {
			printf("Unexpected new line - line %d!\n",row);
			error=true;
			break;
		}
		if(buf[i]==' ' || buf[i]=='\t')
			break;
		instr[instrl++]=buf[i];
		instr[instrl]='\0';
		i++;
	}
	return error;
}
/*
Function: getArguments
Usage:	Gets the arguments at the current row
Arguments:
buf			string buffer of the input file
i			current position in the input file
arg			pointer to the string that is to be returned
argl		pointer to the length of the string
*/
bool getArguments(char* buf,int &i,char* arg,int& argl) {
	bool error=false;
	while(1) {
		if(buf[i]==';') {
			printf("Unexpected comment - line %d!\n",row);
			error=true;
			break;
		}
		if(buf[i]==' ' || buf[i]=='\t' || buf[i]=='\r' || buf[i]=='\n' || i==fsize)
			break;
		arg[argl++]=buf[i];
		arg[argl]='\0';
		i++;
	}
	return error;
}
/*
	===Main Function===
*/
int main(int argc, char* args[]) {
	//memory initialization
	addc=new char*[65536];
	addi=new int[65536];
	char* buf=new char[1048576];
	//opening input file
	if(argc!=2) {
		printf("No file selected!");
		return 0;
	}
	FILE* f;
	f=fopen(args[1],"r");
	if(!f) {
		printf("The selected file could not be opened!");
		return 0;
	}
	//getting the filesize
	fseek(f,0,SEEK_END);
	fsize=ftell(f);
	rewind(f);
	//reading the input file
	fsize=fread(buf,sizeof(char),fsize,f);
	//more memory intialization
	char name[1024];
	int namel;
	char instr[1024];
	int instrl;
	char arg[1024];
	int argl;
	int i=0;
	int curadr=0;
	row=0;
	bool error=false;
	char reg1,reg2,reg3;
	short addrt;
	//First pass
	//Parsing label names and assigning them to memory addresses
	while(i<fsize) {
		row++;
		bool nline=false;
		//resetting strings
		namel=0;
		name[namel]='\0';
		instrl=0;
		instr[instrl]='\0';
		nline=getLabelName(buf,i,name,namel);
		//if there is a label without instruction - error
		if(namel!=0 && nline) {
			if(i!=fsize)
				printf("Unexpected new line - line %d!\n",row);
			break;
		}
		//else if there is a \r or \n or \r\n - pass to the next line
		else if(nline)
			continue;
		//parsing empty characters between label and instruction
		while(i<fsize) {
			if(buf[i]==';') {
				if(namel==0) {
					nline=true;
					break;
				}
				printf("Unexpected comment - line %d!\n",row);
				error=true;
				break;
			}
			else if(buf[i]=='\r' || buf[i]=='\n') {
				printf("Unexpected new row - line %d!\n",row);
				error=true;
				break;
			}
			else if(buf[i]==' ' || buf[i]=='\t')
				i++;
			else
				break;
		}
		if(error)
			break;
		//if we encountered a comment, just go to the end of the line
		//and then pass to the next one
		if(nline) {
			while(1) {
				if(i==fsize)
					break;
				if(buf[i]=='\r' && buf[i+1]=='\n') {
					i+=2;
					break;
				}
				if(buf[i]=='\r' || buf[i]=='\n') {
					i++;
					break;
				}
				i++;
			}
			continue;
		}
		error=getInstrName(buf,i,instr,instrl);
		if(error)
			break;
		//if we found a label, assigning it to the corresponding memory address
		if(namel!=0) {
			addc[adds]=new char[1024];
			strcpy(addc[adds],name);
			addi[adds++]=curadr;
		}
		//checking if the instruction is valid
		if(!strcmp(instr,"add") || !strcmp(instr,"sub") || !strcmp(instr,"mul") || !strcmp(instr,"div") || !strcmp(instr,"cmp") || !strcmp(instr,"inv") || !strcmp(instr,"and") || !strcmp(instr,"or") || !strcmp(instr,"xor") || !strcmp(instr,"shiftl") || !strcmp(instr,"shiftr") || !strcmp(instr,"trap") || !strcmp(instr,"data")) {
			curadr++;
		}
		else if(!strcmp(instr,"lea") || !strcmp(instr,"load") || !strcmp(instr,"store") || !strcmp(instr,"jump") || !strcmp(instr,"jumplt") || !strcmp(instr,"jumple") || !strcmp(instr,"jumpeq") || !strcmp(instr,"jumpne") || !strcmp(instr,"jumpge") || !strcmp(instr,"jumpgt") || !strcmp(instr,"jumpovf") || !strcmp(instr,"jumpnovf") || !strcmp(instr,"jumpco") || !strcmp(instr,"jumpnco")) {
			curadr+=2;
		}
		else {
			printf("Unsupported instruction %s - line %d!\n",instr,row);
			break;
		}
		//skip everything until the end of the line
		while(1) {
			if(i==fsize)
				break;
			if(buf[i]=='\r' && buf[i+1]=='\n') {
				i+=2;
				break;
			}
			if(buf[i]=='\r' || buf[i]=='\n') {
				i++;
				break;
			}
			i++;
		}
	}
	//more memory intialization
	i=0;
	curadr=0;
	row=0;
	error=false;
	//opening output file
	FILE* fp=fopen("assembled.asm","w");
	fprintf(fp,"ASM02");
	while(i<fsize) {
		row++;
		bool nline=false;
		//resetting strings
		namel=0;
		name[namel]='\0';
		instrl=0;
		instr[instrl]='\0';
		argl=0;
		arg[argl]='\0';
		nline=getLabelName(buf,i,name,namel);
		//if there is a label without instruction - error
		if(namel!=0 && nline) {
			if(i!=fsize)
				printf("Unexpected new line - line %d!\n",row);
			break;
		}
		//else just jump to the next line
		else if(nline)
			continue;
		//skipping the empty space between the label and the instruction
		while(i<fsize) {
			if(buf[i]==';') {
				if(namel==0) {
					nline=true;
					break;
				}
				printf("Unexpected comment - line %d!\n",row);
				error=true;
				break;
			}
			else if(buf[i]=='\r' || buf[i]=='\n') {
				printf("Unexpected new row - line %d!\n",row);
				error=true;
				break;
			}
			else if(buf[i]==' ' || buf[i]=='\t')
				i++;
			else
				break;
		}
		if(error)
			break;
		//if we encountered a comment - just skip everything and go to the next line
		if(nline) {
			while(1) {
				if(i==fsize)
					break;
				if(buf[i]=='\r' && buf[i+1]=='\n') {
					i+=2;
					break;
				}
				if(buf[i]=='\r' || buf[i]=='\n') {
					i++;
					break;
				}
				i++;
			}
			continue;
		}
		error=getInstrName(buf,i,instr,instrl);
		if(error)
			break;
		//skipping the empty space between the instruction and the arguments
		while(i<fsize) {
			if(buf[i]==';') {
				printf("Unexpected comment - line %d!\n",row);
				error=true;
				break;
			}
			else if(buf[i]=='\r' || buf[i]=='\n') {
				printf("Unexpected new row - line %d!\n",row);
				error=true;
				break;
			}
			else if(buf[i]==' ' || buf[i]=='\t')
				i++;
			else
				break;
		}
		if(error)
			break;
		//parsing the argument(s)
		error=getArguments(buf,i,arg,argl);
		if(error)
			break;
		//translating the assembly into machine code
		if(!strcmp(instr,"add")) {
			if(!checkargs1(arg,argl,reg1,reg2,reg3)) {
				printf("Invalid number of arguments supplied for add - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"0%c%c%c",reg1,reg2,reg3);
				curadr++;
			}
		}
		else if(!strcmp(instr,"sub")) {
			if(!checkargs1(arg,argl,reg1,reg2,reg3)) {
				printf("Invalid number of arguments supplied for sub - line %d!\n",row);
				error=true;
			}
			else {				
				fprintf(fp,"1%c%c%c",reg1,reg2,reg3);
				curadr++;
			}
		}
		else if(!strcmp(instr,"mul")) {
			if(!checkargs1(arg,argl,reg1,reg2,reg3)) {
				printf("Invalid number of arguments supplied for mul - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"2%c%c%c",reg1,reg2,reg3);
				curadr++;
			}
		}
		else if(!strcmp(instr,"div")) {
			if(!checkargs1(arg,argl,reg1,reg2,reg3)) {
				printf("Invalid number of arguments supplied for div - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"3%c%c%c",reg1,reg2,reg3);
				curadr++;
			}
		}
		else if(!strcmp(instr,"cmp")) {
			if(!checkargs2(arg,argl,reg1,reg2)) {
				printf("Invalid number of arguments supplied for cmp - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"40%c%c",reg1,reg2);
				curadr++;
			}
		}
		else if(!strcmp(instr,"inv")) {
			if(!checkargs1(arg,argl,reg1,reg2,reg3)) {
				printf("Invalid number of arguments supplied for inv - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"5%c%c%c",reg1,reg2,reg3);
				curadr++;
			}
		}
		else if(!strcmp(instr,"and")) {
			if(!checkargs1(arg,argl,reg1,reg2,reg3)) {
				printf("Invalid number of arguments supplied for and - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"6%c%c%c",reg1,reg2,reg3);
				curadr++;
			}
		}
		else if(!strcmp(instr,"or")) {
			if(!checkargs1(arg,argl,reg1,reg2,reg3)) {
				printf("Invalid number of arguments supplied for or - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"7%c%c%c",reg1,reg2,reg3);
				curadr++;
			}
		}
		else if(!strcmp(instr,"xor")) {
			if(!checkargs1(arg,argl,reg1,reg2,reg3)) {
				printf("Invalid number of arguments supplied for xor - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"8%c%c%c",reg1,reg2,reg3);
				curadr++;
			}
		}
		else if(!strcmp(instr,"shiftl")) {
			if(!checkargs1(arg,argl,reg1,reg2,reg3)) {
				printf("Invalid number of arguments supplied for shiftl - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"9%c%c%c",reg1,reg2,reg3);
				curadr++;
			}
		}
		else if(!strcmp(instr,"shiftr")) {
			if(!checkargs1(arg,argl,reg1,reg2,reg3)) {
				printf("Invalid number of arguments supplied for shiftr - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"a%c%c%c",reg1,reg2,reg3);
				curadr++;
			}
		}
		else if(!strcmp(instr,"trap")) {
			if(!checkargs1(arg,argl,reg1,reg2,reg3)) {
				printf("Invalid number of arguments supplied for trap - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"d%c%c%c",reg1,reg2,reg3);
				curadr++;
			}
		}
		else if(!strcmp(instr,"data")) {
			if(arg[0]=='$') {
				if(argl!=5) {
					printf("Invalid number of arguments supplied for data - line %d!\n",row);
					error=true;
				}
				else {
					fprintf(fp,"%c%c%c%c",arg[1],arg[2],arg[3],arg[4]);
					curadr++;
				}
			}
			else {
				int ti=0;
				if(arg[0]=='-') ti++;
				int num=0;
				while(ti<argl) {
					if(arg[ti]<'0' || arg[ti]>'9') {
						error=true;
						break;
					}
					num=num*10+(arg[ti++]-'0');
				}
				if(!error) {
					if(arg[0]=='-') num=-num;
					char temp[64];
					sprintf(temp,"%04hx",(char)(num));
					fprintf(fp,"%c%c%c%c",temp[0],temp[1],temp[2],temp[3]);
					curadr++;
				}
			}
		}
		else if(!strcmp(instr,"lea")) {
			if(!checkargs3(arg,argl,reg1,reg2,addrt)) {
				printf("Invalid number of arguments supplied for lea - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"f%c%c0",reg1,reg2);
				fprintf(fp,"%04hx",addrt);
				curadr+=2;
			}
		}
		else if(!strcmp(instr,"load")) {
			if(!checkargs3(arg,argl,reg1,reg2,addrt)) {
				printf("Invalid number of arguments supplied for load - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"f%c%c1",reg1,reg2);
				fprintf(fp,"%04hx",addrt);
				curadr+=2;
			}
		}
		else if(!strcmp(instr,"store")) {
			if(!checkargs3(arg,argl,reg1,reg2,addrt)) {
				printf("Invalid number of arguments supplied for store - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"f%c%c2",reg1,reg2);
				fprintf(fp,"%04hx",addrt);
				curadr+=2;
			}
		}
		else if(!strcmp(instr,"jump")) {
			if(!checkargs4(arg,argl,reg1,addrt)) {
				printf("Invalid number of arguments supplied for jump - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"f0%c3",reg1);
				fprintf(fp,"%04hx",addrt);
				curadr+=2;
			}
		}
		else if(!strcmp(instr,"jumplt")) {
			if(!checkargs4(arg,argl,reg1,addrt)) {
				printf("Invalid number of arguments supplied for jumplt - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"f1%c3",reg1);
				fprintf(fp,"%04hx",addrt);
				curadr+=2;
			}
		}
		else if(!strcmp(instr,"jumple")) {
			if(!checkargs4(arg,argl,reg1,addrt)) {
				printf("Invalid number of arguments supplied for jumple - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"f2%c3",reg1);
				fprintf(fp,"%04hx",addrt);
				curadr+=2;
			}
		}
		else if(!strcmp(instr,"jumpeq")) {
			if(!checkargs4(arg,argl,reg1,addrt)) {
				printf("Invalid number of arguments supplied for jumpeq - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"f3%c3",reg1);
				fprintf(fp,"%04hx",addrt);
				curadr+=2;
			}
		}
		else if(!strcmp(instr,"jumpne")) {
			if(!checkargs4(arg,argl,reg1,addrt)) {
				printf("Invalid number of arguments supplied for jumpne - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"f4%c3",reg1);
				fprintf(fp,"%04hx",addrt);
				curadr+=2;
			}
		}
		else if(!strcmp(instr,"jumpge")) {
			if(!checkargs4(arg,argl,reg1,addrt)) {
				printf("Invalid number of arguments supplied for jumpge - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"f5%c3",reg1);
				fprintf(fp,"%04hx",addrt);
				curadr+=2;
			}
		}
		else if(!strcmp(instr,"jumpgt")) {
			if(!checkargs4(arg,argl,reg1,addrt)) {
				printf("Invalid number of arguments supplied for jumpgt - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"f6%c3",reg1);
				fprintf(fp,"%04hx",addrt);
				curadr+=2;
			}
		}
		else if(!strcmp(instr,"jumpovf")) {
			if(!checkargs4(arg,argl,reg1,addrt)) {
				printf("Invalid number of arguments supplied for jumpovf - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"f7%c3",reg1);
				fprintf(fp,"%04hx",addrt);
				curadr+=2;
			}
		}
		else if(!strcmp(instr,"jumpnovf")) {
			if(!checkargs4(arg,argl,reg1,addrt)) {
				printf("Invalid number of arguments supplied for jumpnovf - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"f8%c3",reg1);
				fprintf(fp,"%04hx",addrt);
				curadr+=2;
			}
		}
		else if(!strcmp(instr,"jumpco")) {
			if(!checkargs4(arg,argl,reg1,addrt)) {
				printf("Invalid number of arguments supplied for jumpco - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"f9%c3",reg1);
				fprintf(fp,"%04hx",addrt);
				curadr+=2;
			}
		}
		else if(!strcmp(instr,"jumpnco")) {
			if(!checkargs4(arg,argl,reg1,addrt)) {
				printf("Invalid number of arguments supplied for jumpnco - line %d!\n",row);
				error=true;
			}
			else {
				fprintf(fp,"fa%c3 ",reg1);
				fprintf(fp,"%04hx",addrt);
				curadr+=2;
			}
		}
		if(error)
			break;
		//skipping everything until the end of the line
		while(1) {
			if(i==fsize)
				break;
			if(buf[i]=='\r' && buf[i+1]=='\n') {
				i+=2;
				break;
			}
			if(buf[i]=='\r' || buf[i]=='\n') {
				i++;
				break;
			}
			i++;
		}
	}
	//closing files
	fclose(fp);
	fclose(f);
	//freeing memory
	delete[] buf;
	delete[] addi;
	for(i=0;i<adds;i++)
		delete[] addc[i];
	delete[] addc;
	return 0;
}