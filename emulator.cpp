#include<stdio.h>
#include<string.h>
#include<windows.h>
int hex2dec(char c) {
	int resp[256];
	resp['0']=0;
	resp['1']=1;
	resp['2']=2;
	resp['3']=3;
	resp['4']=4;
	resp['5']=5;
	resp['6']=6;
	resp['7']=7;
	resp['8']=8;
	resp['9']=9;
	resp['a']=10;
	resp['b']=11;
	resp['c']=12;
	resp['d']=13;
	resp['e']=14;
	resp['f']=15;
	return resp[c];
}
double PCFreq=0.00;
__int64 CounterStart=0;
void start_timer() {
	LARGE_INTEGER li;
	if(!QueryPerformanceFrequency(&li))
		return;
	PCFreq=double(li.QuadPart)/1000.0;
	QueryPerformanceCounter(&li);
	CounterStart=li.QuadPart;
}
float end_timer(int exec) {
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	float time=float(double(li.QuadPart-CounterStart)/PCFreq);
	return time;
}
int main(int argc, char* args[]) {
	if(argc!=2) {
		printf("No file selected!");
		return 0;
	}
	static unsigned char memory[65536][4];
	int memorysize=0;
	char output[65536];
	int outputsize=0;
	char temp[64];
	FILE* f=fopen(args[1],"r");
	fread(temp,sizeof(char),5,f);
	temp[5]='\0';
	if(strcmp(temp,"ASM02")) {
		printf("Invalid file format!");
		fclose(f);
		return 0;
	}
	int i=0;
	while(1) {
		if(fread(temp,sizeof(char),4,f)<4)
			break;
		memory[i][0]=hex2dec(temp[0]);
		memory[i][1]=hex2dec(temp[1]);
		memory[i][2]=hex2dec(temp[2]);
		memory[i][3]=hex2dec(temp[3]);
		printf("%04x %x%x%x%x\n",i,memory[i][0],memory[i][1],memory[i][2],memory[i][3]);
		i++;
	}
	printf("----\n");
	memorysize=i;
	fclose(f);
	static short registers[16];
	int instr=0;
	int executed=0;
	int cmp1,cmp2;
	start_timer();
	while(instr<65536) {
		executed++;
		if(memory[instr][0]==0)
			registers[memory[instr][1]]=registers[memory[instr][2]]+registers[memory[instr][3]];
		else if(memory[instr][0]==1)
			registers[memory[instr][1]]=registers[memory[instr][2]]-registers[memory[instr][3]];
		else if(memory[instr][0]==2)
			registers[memory[instr][1]]=registers[memory[instr][2]]*registers[memory[instr][3]];
		else if(memory[instr][0]==3)
			registers[memory[instr][1]]=registers[memory[instr][2]]/registers[memory[instr][3]];
		else if(memory[instr][0]==4) {
			cmp1=registers[memory[instr][2]];
			cmp2=registers[memory[instr][3]];
		}
		else if(memory[instr][0]==5)
			registers[memory[instr][1]]=registers[memory[instr][2]]^0b1111;
		else if(memory[instr][0]==6)
			registers[memory[instr][1]]=registers[memory[instr][2]]&registers[memory[instr][3]];
		else if(memory[instr][0]==7)
			registers[memory[instr][1]]=registers[memory[instr][2]]|registers[memory[instr][3]];
		else if(memory[instr][0]==8)
			registers[memory[instr][1]]=registers[memory[instr][2]]^registers[memory[instr][3]];
		else if(memory[instr][0]==9)
			registers[memory[instr][1]]=registers[memory[instr][2]]<<registers[memory[instr][3]];
		else if(memory[instr][0]==10)
			registers[memory[instr][1]]=registers[memory[instr][2]]>>registers[memory[instr][3]];
		else if(memory[instr][0]==13) {
			if(memory[instr][1]==0)
				break;
			else if(registers[memory[instr][1]]==2) {
				int startt=registers[memory[instr][2]];
				int endt=startt+registers[memory[instr][3]];
				for(int t=startt;t<endt;t++) {
					output[outputsize++]=memory[t][0]*4096+memory[t][1]*256+memory[t][2]*16+memory[t][3];
					output[outputsize]='\0';
				}
			}
		}
		else if(memory[instr][0]==15) {
			if(memory[instr][3]==0)
				registers[memory[instr][1]]=(memory[instr+1][0]*4096+memory[instr+1][1]*256+memory[instr+1][2]*16+memory[instr+1][3])+registers[memory[instr][2]];
			else if(memory[instr][3]==1) {
				int addr=(memory[instr+1][0]*4096+memory[instr+1][1]*256+memory[instr+1][2]*16+memory[instr+1][3])+registers[memory[instr][2]];
				registers[memory[instr][1]]=memory[addr][0]*4096+memory[addr][1]*256+memory[addr][2]*16+memory[addr][3];
			}
			else if(memory[instr][3]==2) {
				int mem0,mem1,mem2,mem3,t;
				t=registers[memory[instr][1]];
				mem0=t/4096;
				mem1=(t/256)%16;
				mem2=(t/16)%16;
				mem3=t%16;
				int addr=(memory[instr+1][0]*4096+memory[instr+1][1]*256+memory[instr+1][2]*16+memory[instr+1][3])+registers[memory[instr][2]];
				memory[addr][0]=mem0;
				memory[addr][1]=mem1;
				memory[addr][2]=mem2;
				memory[addr][3]=mem3;
			}
			else if(memory[instr][3]==3) {
				if(memory[instr][1]==0 || (memory[instr][1]==1 && cmp1<cmp2) || (memory[instr][1]==2 && cmp1<=cmp2) || (memory[instr][1]==3 && cmp1==cmp2) || (memory[instr][1]==4 && cmp1!=cmp2) || (memory[instr][1]==5 && cmp1>=cmp2) || (memory[instr][1]==6 && cmp1>cmp2)) {
					int addr=(memory[instr+1][0]*4096+memory[instr+1][1]*256+memory[instr+1][2]*16+memory[instr+1][3])+registers[memory[instr][2]];
					instr=addr;
					continue;
				}
			}
			instr++;
		}
		instr++;
	}
	float time=end_timer(executed);
	printf("%s",output);
	printf("Ops executed: %d\n",executed);
	printf("Execution time: %0.2f ms\n",time);
	printf("Instructions per second: %d\n",int(executed*1000.0/time));
	for(i=0;i<16;i++) {
		printf("R%d = %d\n",i,registers[i]);
	}
	return 0;
}